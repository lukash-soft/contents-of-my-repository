# Contents of my repository

## Personal and online training manager

My greatest achievement as a java developer. The app was created along with my mentor - Maciej Folik.
He introduced me to Java world with this project. It is about managing your online and stationary clients as a personal
trainer.

##### [Java part - back-end](https://gitlab.com/lukash-soft/workout-backend)

##### [Angular part - front-end](https://gitlab.com/lukash-soft/workout-frontend)

## [CRED recruitment task](https://gitlab.com/lukash-soft/credrecruitmenttask)

This project was a recruitment task to the CRED company.
It should recognize the file using magic numbers for at least three types of files:
-PDF,GIF, JPG, TXT.

## [FizzBuzz Repository](https://gitlab.com/lukash-soft/fizzbuzzgame)

The FizzBuzz problem is a classic test given in coding interviews.
The task is simple: Print integers one-to-N, but print “Fizz”
if an integer is divisible by three, “Buzz” if an integer is divisible
by five, and “FizzBuzz” if an integer is divisible by both three and five.

## [Learning Exercises](https://gitlab.com/lukash-soft/LearningExercises)

This is a project where I store my simple recruitment tasks which improve my
abstract thinking and math. I do this in order to understand work on arrays,
recursion, collections, loops, functional programming.

## [Rest template repository](https://gitlab.com/lukash-soft/resttemplateplayground)

This is a project where I test calling another api and configuring RestTemplate class.

## [UG recruitment task](https://gitlab.com/lukash-soft/ug-recruitment-task)

This is a project where I call NBP Api to get currency rate. The purpose of this
task is to count the total price of computers converting the price from USD to PLN.
The additional task was to generate invoice in a XML file.

## [Simple on-line shop repository](https://gitlab.com/lukash-soft/SimpleOnlineShopBackEnd)

This project was done to teach me the basics of Spring Security using different
Spring books.

## [Vat Api](https://gitlab.com/lukash-soft/vatapi)

This project is about checking if a given account is on a Polish Vat whitelist.
It includes simple REST Api.

## [xCodeRecruitmentTask](https://gitlab.com/lukash-soft/xcoderecruitmenttask)

This is a task from XCode company which is about:

1. Creating a GET endpoint
2. Creating a POST endpoint - sorting list of numbers.
3. Creating a POST endpoint - getting the currency rate after currency code
   as an input.

## [SQL Playground](https://gitlab.com/lukash-soft/sql-playground)

This project taught me how to configure jdbc and hibernate. 
